<?php

include_spip('verifier/fichiers');

/**
 * Chercher les options de la vérification de type 'fichiers'
 * Soit directement dans #ENV{verifier} soit dans un de ses indexes
 *
 * @param $verifier
 * @return array
 */
function chercher_verifier_fichiers($verifier = null): array {
	if (is_array($verifier['type'] ?? null) && $verifier['type'] === 'fichiers') {
		return $verifier;
	}
	foreach ($verifier as $item) {
		if (is_array($item ?? null) && ($item['type'] ?? null) === 'fichiers') {
			return $item;
		}
	}
	return [];
}
