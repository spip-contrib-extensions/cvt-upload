<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}
include_spip('inc/cvtupload');
include_spip('inc/saisies');
include_spip('inc/autoriser');

function formulaires_test_upload_saisies() {
	static $saisies;
	if (!$saisies == null) {
		return $saisies;
	}
	$saisies = [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'tromperie',
				'label' => 'Si c\'est rempli, on se trompe',
				'defaut' => _request('tromperie')
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'pdfs',
				'label' => 'Plusieurs fichiers PDF dans un même champ',
				'nb_fichiers' => 2,
				'obligatoire' => 'oui'
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => [
					'mime' => 'specifique',
					'mime_specifique' => ['application/pdf']
				]
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'fichier_tout_mime',
				'label' => 'Un fichier, n\'importe quel type MIME accepté par SPIP',
				'nb_fichiers' => 1
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => ['mime' => 'tout_mime']
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'fichier_image_web',
				'label' => 'Un fichier de type image web',
				'nb_fichiers' => 1
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => ['mime' => 'image_web']
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'fichier_leger',
				'label' => 'Un fichier léger (≤ 10 kio)',
				'nb_fichiers' => 1
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => ['taille_max' => 10]
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'image_web_pas_trop_grande',
				'label' => 'Une image web pas plus grande que 1024 px de largeur et 640 px de hauteur',
				'nb_fichiers' => 1
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => [
					'mime' => 'image_web',
					'dimension_max' => [
						'largeur' => 1024,
						'hauteur' => 640
					]
				]
			]
		],
		[
			'saisie' => 'fichiers',
			'options' => [
				'nom' => 'image_web_pas_trop_grande_rotation',
				'label' => 'Une image web pas plus grande que 1024 px de largeur et 640 px de hauteur, ou l\'inverse',
				'nb_fichiers' => 1
			],
			'verifier' => [
				'type' => 'fichiers',
				'options' => [
					'mime' => 'image_web',
					'dimension_max' => [
						'largeur' => 1024,
						'hauteur' => 640,
						'autoriser_rotation' => true
					]
				]
			]
		]
	];
	return $saisies;
}

function formulaires_test_upload_saisie_charger() {
	// formulaires de test utilisables par les webmestres uniquement
	if (!autoriser('webmestre')) {
		return false;
	}

	$contexte = [
		'mes_saisies' => formulaires_test_upload_saisies()
	];

	return $contexte;
}

function formulaires_test_upload_saisie_fichiers() {
	return array_keys(saisies_lister_avec_type(formulaires_test_upload_saisies(), 'fichiers'));
}

function formulaires_test_upload_saisie_verifier() {
	$erreurs = [];

	if (_request('tromperie')) {
		$erreurs['tromperie'] = 'Il ne fallait rien remplir.';
	}

	// Vérifier les autres saisies (de type fichiers)
	$saisies = formulaires_test_upload_saisies();
	$saisies_verifier = saisies_verifier($saisies, true);

	// fusionner avec nos précedentes erreurs
	$erreurs = array_merge($erreurs, $saisies_verifier);

	return $erreurs;
}

function formulaires_test_upload_saisie_traiter() {
	$retours = ['message_ok' => 'Il ne se passe rien.'];

	$fichiers = _request('_fichiers');
	var_dump($_FILES);
	var_dump($fichiers);

	return $retours;
}
