<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Age maximum des fichiers dans le dossier temporaire
 **/
if (!defined('_CVTUPLOAD_AGE_MAX')) {
	define('_CVTUPLOAD_AGE_MAX', 6 * 3600);
}

/**
 * Nombre maximum de fichiers dans le dossier temporaire
 **/
if (!defined('_CVTUPLOAD_MAX_FILES')) {
	define('_CVTUPLOAD_MAX_FILES', 200);
}


include_spip('base/abstract_sql');

/**
 * Calculer un uniqid du formulaire et de ses args pour ne pas se melanger les pinceaux entre plusieurs formulaires
 * @param $form
 * @param $args
 * @return string
 */
function cvtupload_formulaire_id($form, $args) {
	if ($identifier_args = charger_fonction('identifier', "formulaires/$form", true)) {
		$args = call_user_func_array($identifier_args, $args);
	}

	return 'cvtupload' . md5(json_encode(['form' => $form, 'args' => $args]));
}

/**
 * Chercher si des champs fichiers ont été déclarés dans le fichier formulaires/xxx.php
 * Sert de condition preliminaire pour les pipelines formulaire_charger, formulaire_verifier et formulaire_fond du plugin
 *
 * @param string $form
 *     le nom du formulaire
 * @param array $args
 *     - l'id de l'objet
 *
 * @return array
 *     valeur(s) de l'attribut 'name' du ou des input de type file dans formulaires/xxx.html
 */
function cvtupload_chercher_fichiers($form, $args) {
	$fichiers = [];

	// S'il existe une fonction de fichiers dédiée à ce formulaire
	if ($fonction_fichiers = charger_fonction('fichiers', 'formulaires/' . $form, true)) {
		// ne pas transmettre des arguments nommés à $fonction_fichiers s'ils ne sont pas dans sa signature
		$reflection_fonction = new ReflectionFunction($fonction_fichiers);
		$fonction_fichiers_args = [];
		foreach ($reflection_fonction->getParameters() as $param) {
			$fonction_fichiers_args[] = $param->getName();
		}
		foreach ($args as $key => $value) {
			if (is_string($key) && !in_array($key, $fonction_fichiers_args)) {
				unset($args[$key]);
			}
		}
		$fichiers = call_user_func_array($fonction_fichiers, $args);
	}

	// Dans tous les cas on applique le pipeline, si un plugin veut ajouter des choses
	$fichiers = pipeline(
		'formulaire_fichiers',
		['args' => ['form' => $form, 'args' => $args], 'data' => $fichiers]
	);

	return $fichiers;
}

/**
 * Génére le HTML de chaque fichier déjà uploadé
 *
 * @param array $infos_fichiers
 * 		Tableau contenant les informations pour chaque champ de fichier
 * @return array
 * 		Retourne un tableau avec pour chaque champ une clé contenant le HTML
 **/
function cvtupload_generer_html($infos_fichiers = null) {
	static $html_fichiers = [];
	// Si on a des infos de fichiers, on va re-générer du HTML
	if ($infos_fichiers and is_array($infos_fichiers)) {
		foreach ($infos_fichiers as $champ => $fichier) {
			// Si c'est un champ unique
			if (isset($fichier['name'])) {
				$html_fichiers[$champ] = recuperer_fond(
					'formulaires/inc-cvtupload-fichier',
					array_merge($fichier, [
						'crochets' => "[$champ]",
						'champ'    => "$champ",
					])
				);
			} // Sinon c'est un champ multiple
			else {
				$html_fichiers[$champ] = [];
				foreach ($fichier as $cle => $infos) {
					$html_fichiers[$champ][$cle] = recuperer_fond(
						'formulaires/inc-cvtupload-fichier',
						array_merge($infos, [
							'crochets' => "[$champ][$cle]",
							'champ'    => $champ . "[$cle]",
						])
					);
				}
			}
		}
	}

	return $html_fichiers;
}


/**
 * Return le hidden de chaque fichier déjà uploadé
 *
 * @param array $infos_fichiers
 * 		Tableau contenant les informations pour chaque champ de fichier
 * @return string
 * 		Retourne un tableau avec pour chaque champ une clé contenant le HTML
 **/
function cvtupload_generer_hidden($infos_fichiers = null) {
	static $hidden_fichiers = '';

	// Si on a des infos de fichiers, on va re-générer du HTML
	if ($infos_fichiers and is_array($infos_fichiers)) {
		foreach ($infos_fichiers as $champ => $fichier) {
			// Si c'est un champ unique
			if (isset($fichier['name'])) {
				$crochets = "[$champ]";
				$infos_encodees = $fichier['infos_encodees'];
				$hidden_fichiers .= "<input type='hidden' name='cvtupload_fichiers_precedents$crochets' value='$infos_encodees' />";
			} // Sinon c'est un champ multiple
			else {
				$html_fichiers[$champ] = [];
				foreach ($fichier as $cle => $infos) {
					$crochets = "[$champ][$cle]";
					$infos_encodees = $infos['infos_encodees'];
					$hidden_fichiers .= "<input type='hidden' name='cvtupload_fichiers_precedents$crochets' value='$infos_encodees' />";
				}
			}
		}
	}

	return $hidden_fichiers;
}

/**
 * Déplace un fichier uploadé dans un endroit temporaire et retourne des informations dessus.
 * @param array $fichier
 * 		Le morceau de $_FILES concernant le ou les fichiers
 * @param string $repertoire
 * 		Chemin de destination des fichiers
 * @param string $form
 * 		Formulaire d'où ça vient
 * @param bool $deplacer
 *		Mettre a False pour se contenter de copier
 * @return array
 * 		Retourne un tableau d'informations sur le fichier ou un tableau de tableaux si plusieurs fichiers. Ce tableau est compatible avec l'action "ajouter_un_fichier" de SPIP.
 **/
function cvtupload_deplacer_fichier($fichier, $repertoire, $form, $deplacer = true) {
	include_spip('inc/documents');
	include_spip('action/ajouter_documents');
	include_spip('verifier/fichiers');

	$vignette_par_defaut = charger_fonction('vignette', 'inc/');
	$infos = [];
	// On commence par nettoyer le dossier
	cvtupload_nettoyer_repertoire($repertoire);

	// Si on est sur un upload de type fichier unique, on reformate le tableau pour faire comme si on était en fichiers multiples
	if (!is_array($fichier['name'])) {
		$fichier_unique = true;
		$fichier_nouveau = [];
		foreach ($fichier as $champ => $valeur) {
			$fichier_nouveau[$champ] = [$valeur];
		}
		$fichier = $fichier_nouveau;
	} else {
		$fichier_unique = false;
	}

	foreach ($fichier['name'] as $cle => $nom) {
		// Si le fichier a bien un nom et qu'il n'y a pas d'erreur associé à ce fichier
		if (
			($nom != null)
			and ($fichier['error'][$cle] == 0)
			// Et qu'on génère bien un nom de fichier aléatoire pour déplacer le fichier
			and $chemin_aleatoire = tempnam($repertoire, $form . '_')
		) {
			$extension = strtolower(pathinfo($fichier['name'][$cle], PATHINFO_EXTENSION));

			// Corriger les équivalences d'extensions
			$extension_old = $extension;
			$extension = corriger_extension($extension);
			$mime_type = sql_getfetsel(
				'mime_type',
				'spip_types_documents',
				'extension = ' . sql_quote($extension)
			);
			$nom = str_replace(".$extension_old", ".$extension", $nom);

			if (in_array($mime_type, verifier_fichier_mime_type_image_web())) {
				$chemin_aleatoire .= ".$extension";
			}
			// Déplacement du fichier vers le dossier de réception temporaire + récupération d'infos
			if (deplacer_fichier_upload($fichier['tmp_name'][$cle], $chemin_aleatoire, $deplacer)) {
				$infos[$cle]['tmp_name'] = $chemin_aleatoire;
				$infos[$cle]['name'] = $nom;
				$infos[$cle]['extension'] = $extension;
				// si image on fait une copie avec l'extension pour pouvoir avoir l'image réduite en vignette
				if (in_array($mime_type, verifier_fichier_mime_type_image_web())) {
					$infos[$cle]['vignette'] = $chemin_aleatoire;
				} else {
					$infos[$cle]['vignette'] = $vignette_par_defaut($infos[$cle]['extension'], false, true);
				}
				//On récupère le type MIME du fichier aussi
				$infos[$cle]['mime'] = cvt_upload_determiner_mime($fichier['type'][$cle], $infos[$cle]['extension']);
				$infos[$cle]['taille'] = $fichier['size'][$cle];
				// On stocke des infos sur le formulaire
				$infos[$cle]['form'] = $form;
				$infos[$cle]['infos_encodees'] = encoder_contexte_ajax($infos[$cle], $form);
			}
		}
	}

	if (!empty($infos) and $fichier_unique == true) {
		$infos = $infos[0];
	}
	return $infos;
}

/**
 * Modifier $_FILES pour que le nom et le chemin du fichier temporaire
 * correspondent à ceux qu'on a défini dans cvtupload_deplacer_fichier().
 * Cela permet aux traitements ultérieurs
 * de ne pas avoir à se préoccuper de l'emploi ou non de cvtupload.
 *
 * @param $infos_fichiers
 *  Information sur les fichiers tels que déplacés par cvtupload_deplacer_fichier()
 * @return void
**/
function cvtupload_modifier_files($infos_fichiers) {
	foreach ($infos_fichiers as $champ => $description) {
		if (isset($description['tmp_name'])) {//Upload unique
			 $_FILES[$champ] = [];//On surcharge tout la description $_FILES pour ce champ.  Dans tous les cas les infos ont été stockées dans $description
			 $_FILES[$champ]['name'] = $description['name'];
			 $_FILES[$champ]['tmp_name'] = $description['tmp_name'];
			 $_FILES[$champ]['type'] = $description['mime'];
			 $_FILES[$champ]['error'] = 0; //on fait comme s'il n'y avait pas d'erreur, ce qui n'est pas forcément vrai…
			 $_FILES[$champ]['size'] = $description['taille'];
		} else {//Upload multiple
			//On surcharge tout la description $_FILES pour ce champ. Dans tous les cas les infos ont été stockées dans $description
			if (isset($_FILES[$champ])) {
				$old_FILES_champ = $_FILES[$champ];
			} else {
				$old_FILES_champ = [];
			}
			$_FILES[$champ]['name'] = [];
			$_FILES[$champ]['tmp_name'] = [];
			$_FILES[$champ]['type'] = [];
			$_FILES[$champ]['error'] = [];
			$_FILES[$champ]['size'] = [];
			// Et on re-rempli à partir de $description
			foreach ($description as $fichier_individuel => $description_fichier_individuel) {
				$_FILES[$champ]['name'][$fichier_individuel] = $description_fichier_individuel['name'];
				$_FILES[$champ]['tmp_name'][$fichier_individuel] = $description_fichier_individuel['tmp_name'];
				$_FILES[$champ]['type'][$fichier_individuel] = $description_fichier_individuel['mime'];
				$_FILES[$champ]['error'][$fichier_individuel] = 0; //on fait comme s'il n'y avait pas d'erreur, ce qui n'est pas forcément vrai…
				$_FILES[$champ]['size'][$fichier_individuel] = $description_fichier_individuel['taille'];
			}
			// Si on vient d'envoyer un ou plusieur $champ[] vide, on les rajoute dans notre nouveau $FILES
			if (isset($old_FILES_champ['error']) and is_array($old_FILES_champ['error'])) {
				foreach ($old_FILES_champ['error'] as $id_fichier_individuel => $error_fichier_individuel) {
					if ($error_fichier_individuel != 0 and !isset($infos_fichiers[$champ][$id_fichier_individuel])) {//Uniquement les erreurs
						$_FILES[$champ]['name'][$id_fichier_individuel] = $old_FILES_champ['name'][$id_fichier_individuel];
						$_FILES[$champ]['tmp_name'][$id_fichier_individuel] = $old_FILES_champ['tmp_name'][$id_fichier_individuel];
						$_FILES[$champ]['type'][$id_fichier_individuel] = $old_FILES_champ['type'][$id_fichier_individuel];
						$_FILES[$champ]['error'][$id_fichier_individuel] = $old_FILES_champ['error'][$id_fichier_individuel];
						$_FILES[$champ]['size'][$id_fichier_individuel] = $old_FILES_champ['size'][$id_fichier_individuel];
					}
				}
			}
			// On remet de l'ordre dans champ dans chaque tableau correspondant à une propriété de $_FILES, histoire d'avoir 0,1,2,3 et pas 3,1,0,2
			foreach ($_FILES[$champ] as $propriete => $valeurs_propriete) {
				ksort($valeurs_propriete);
				$_FILES[$champ][$propriete] = $valeurs_propriete;
			}
		}
	}
}

/**
 * Nettoyer $_FILES pour effacer les entrées dont on a vérifié qu'elle ne répondaient pas à certains critères
 *
 * @param string $champ
 *	Le nom du champ concerné dans $_FILES
 * @param string[]|string $erreurs
 * 	Si un upload multiple, un tableau des $erreurs avec comme clés les numéros des fichiers à supprimer dans $_FILES[$champ]
 * 	Si un upload unique, une chaîne, qui si non vide, indique qu'il faut effacer le $_FILE[$champ]
 * @return void
**/
function cvtupload_nettoyer_files_selon_erreurs($champ, $erreurs) {
	if (is_array($erreurs)) { // cas d'upload multiple
		foreach ($erreurs as $cle => $erreur) {
			foreach ($_FILES[$champ] as $propriete => $valeur) {
				unset($_FILES[$champ][$propriete][$cle]);
			}
		}
	} elseif ($erreurs != '') { // cas d'upload unique avec erreur
		unset($_FILES[$champ]);
	}
}

/**
 * Détermine un MIME lorsque les informations de PHP sont imprécises.
 * Par exemple PHP considère qu'un fichier .tex est de MIME application/octet-stream
 * Ce qui n'est absolument pas utilse
 * @param string $mime_suppose
 * @param string $extension
 * @return string $mime_trouve
**/
function cvt_upload_determiner_mime($mime_suppose, $extension) {
	if (!in_array($mime_suppose, ['text/plain', '', 'application/octet-stream'])) { // si on a un mime précis, on le renvoie, tout simplement
		return $mime_suppose;
	}
	$mime_spip = sql_getfetsel('mime_type', 'spip_types_documents', 'extension=' . sql_quote($extension));
	if ($mime_spip) {
		return $mime_spip;
	} else {
		return $mime_suppose;
	}
}

/**
 * Nettoyer un répertoire suivant l'age et le nombre de ses fichiers
 *
 * @param string $repertoire
 * 		Répertoire à nettoyer
 * @param int $age_max
 * 		Age maxium des fichiers en seconde
 * @param int $max_files
 * 		Nombre maximum de fichiers dans le dossier
 * @return void
 **/
function cvtupload_nettoyer_repertoire($repertoire, $age_max = _CVTUPLOAD_AGE_MAX, $max_files = _CVTUPLOAD_MAX_FILES) {
	include_spip('inc/flock');

	// Si on entre bien dans le répertoire
	if ($ressource_repertoire = opendir($repertoire)) {
		$fichiers = [];

		// On commence par supprimer les plus vieux
		while ($fichier = readdir($ressource_repertoire)) {
			if (!in_array($fichier, ['.', '..', '.ok'])) {
				$chemin_fichier = $repertoire . $fichier;

				if (is_file($chemin_fichier) and !jeune_fichier($chemin_fichier, $age_max)) {
					supprimer_fichier($chemin_fichier);
				} else {
					$fichiers[@filemtime($chemin_fichier) . '_' . rand()] = $chemin_fichier;
				}
			}
		}

		// On trie les fichiers par ordre de leur date
		ksort($fichiers);

		// Puis s'il reste trop de fichiers, on supprime le surplus
		$nb_fichiers = count($fichiers);
		if ($nb_fichiers > $max_files) {
			$nb_a_supprimer = $nb_fichiers - $max_files - 1;

			while ($nb_a_supprimer) {
				$fichier = array_shift($fichiers);
				supprimer_fichier($fichier);
				$nb_a_supprimer--;
			}
		}
	}
}


/**
 * Regarde le tableau des _FILES sous sa forme
 *  'fichiers_1' => array(
 *		'name' => array(0 =>),
 *		'error' => array(0 =>),
 *	et renvoie une version utilisable pour afficher une vue
 *		'fichiers_array(
 *			0 => array(
 *				'nom' => ,
 *				'extension' =>,
 *				'vignette' =>,
 *				'taille' =>
 *				'mime' =>
 *			)
 *	@return array
**/
function cvtupload_vue_from_FILES() {
	if (empty($_FILES)) {
		return [];
	}
	$tab = [];
	$vignette_par_defaut = charger_fonction('vignette', 'inc/');
	foreach ($_FILES as $nom => $valeur) {
		$tab[$nom] = [];
		foreach ($valeur as $cle_logique => $sous_saisie) {
			foreach ($sous_saisie as $numero => $valeur_sous_saisie) {
				if (!isset($tab[$nom][$numero])) {
					$tab[$nom][$numero] = [
						'nom' => $valeur['name'][$numero],
						'extension' => pathinfo($valeur['name'][$numero], PATHINFO_EXTENSION),
						'taille' => pathinfo($valeur['size'][$numero], PATHINFO_EXTENSION),
						'mime' => pathinfo($valeur['type'][$numero], PATHINFO_EXTENSION),
						'vignette' => $vignette_par_defaut(pathinfo($valeur['name'][$numero], PATHINFO_EXTENSION), false)
					];
				}
			}
		}
	}
	return $tab;
}
