# Changelog

## 2.2.0 - 2024-02-25

### Changed

- Compatible SPIP 4.1 et supérieur seulement

### Fixed

- #19 Utiliser tous les mimes types d'images web

## 2.1.7 - 2025-02-10

### Fixed

- #11 Correction d'une fatale `Unknown named parameter $type` quand on utilise `formulaires_editer_objet_*` dans le
  formulaire

## 2.1.6 - 2024-06-09

- Compatible SPIP 4++

## 2.1.5 - 2024-04-24

### Fixed

- `var_dump()` supprimés dans `cvtupload_pipelines.php`

## 2.1.4 - 2024-04-24

### Fixed

- spip-contrib-extensions/formidable#240 Faire fonctionner la saisie `fichiers` correctement dans un contexte AJAX

### Changed

- La gestion des déplacements / rememoration des fichiers se fait dès réception du formulaire, et non après vérification

## 2.1.3 - 2023-12-30

### Fixed

- Classe `.editer-label` sur le `legend` de la saisie `fichiers`

## 2.1.2 - 2023-02-28

### Changed

- Compatible SPIP 4.2

## 2.1.1 - 2022-12-01

### Changed

- #14 La saisie `type_mime` est rapatriée dans le plugin `saisies`

## 2.1.0 - 2022-11-21

### Changed

- #13 Constructeur de formulaire : utiliser le nouveau pipeline du plugin saisies intitulé `saisies_verifier_lister_disponibles`

## 2.0.1 - 2022-07-27

### Added

- Constructeur de saisie : documentation des options de dev

### Changed

- Constructeur de saisie : `afficher_si` dans onglet à part (cf. Saisies v4.4.0)
- Sécurisation des formulaires de test réservés aux webmestres
