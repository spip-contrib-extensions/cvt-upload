<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cvtupload?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// E
	'erreur' => 'Errore sul file (@erreur@)',
	'erreur_fichier_supprime' => 'Hai eliminato il file « @nom@ »; questo è il motivo per cui offriamo nuovamente questo modulo.',

	// R
	'remplacer_fichier' => 'Sostituisci questo file',

	// S
	'saisie_fichiers_explication' => 'Invia uno o più file, assicurandoti di non doverli inviare nuovamente se gli altri campi del modulo non vengono convalidati.',
	'saisie_fichiers_label_fichiers_individuels_label' => 'Utilizzare un’etichetta per il campo di ogni singolo file?',
	'saisie_fichiers_nb_fichiers_explication' => 'Ciò determina il numero di campi di caricamento file disponibili.',
	'saisie_fichiers_nb_fichiers_label' => 'Numero massimo di file da inviare',
	'saisie_fichiers_plusieur_label' => 'File numero @nb@',
	'saisie_fichiers_titre' => 'File(s)',
	'saisie_fichiers_unseul_label' => 'File singolo',
	'supprimer_fichier' => 'Elimina questo file',
];
