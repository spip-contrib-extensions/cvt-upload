<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cvtupload?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// E
	'erreur' => 'Datei(@erreur@) fehlerhaft',
	'erreur_fichier_supprime' => 'Sie haben die Datei « @nom@ » gelöscht. Aus diesem Grund wird dieses  Formular erneut angezeigt.',

	// R
	'remplacer_fichier' => 'Datei ersetzen',

	// S
	'saisie_fichiers_explication' => 'Eine oder mehrere Dateien hochladen. Es wird sichergestellt, dass sie nicht erneut übertragen werden müssen, falls die Anderen Felder des Formulars nicht bestätigt werden.',
	'saisie_fichiers_label_fichiers_individuels_label' => 'Jedes Dateifeld mit einer eigenen Bezeichnung versehen?',
	'saisie_fichiers_nb_fichiers_explication' => 'Hier können Sie die Anzahl der Felder für hochzuladende Dateien einstellen.',
	'saisie_fichiers_nb_fichiers_label' => 'Höchste Anzahl der zu übertragenden Dateien',
	'saisie_fichiers_plusieur_label' => 'Datei Nr. @nb@',
	'saisie_fichiers_titre' => 'Datei/en',
	'saisie_fichiers_unseul_label' => 'Eine Datei',
	'supprimer_fichier' => 'Datei löschen',
];
