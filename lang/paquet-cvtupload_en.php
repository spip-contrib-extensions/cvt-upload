<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cvtupload?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cvtupload_description' => 'Provides tools to load files properly from a CVT form.',
	'cvtupload_slogan' => 'Download properly files from a CVT form',
];
