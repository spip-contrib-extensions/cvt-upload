<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cvtupload?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'cvtupload_description' => 'Werkzeug zum Hochladen per CVT Formular.',
	'cvtupload_slogan' => 'Dateien per CVT-Formular hochladen',
];
