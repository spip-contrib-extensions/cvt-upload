<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cvtupload?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cvtupload_description' => 'Fornisce strumenti per aiutare a caricare i file in modo pulito da un modulo CVT.',
	'cvtupload_slogan' => 'Carica i file in modo pulito da un modulo CVT',
];
