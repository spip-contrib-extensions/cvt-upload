<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Génère les tests js pour les saisies fichiers
 * Permet de tests @fichiers_1@:TOTAL > 3
 * @param array $parse le parsage du test
 * @param array $saisies_form l'ensemble des saisies du formulaire, triées par nom
 * @return string : du code JS qui peut passer par eval.
 * Note : comme le besoin est limitée, on génère encore du JS dynamique, plutôt qu'un JS global qu'on chargerait systématiquement :
 *   - alléger le chargement dans 90% des case
 *   - flemme de Maïeul d'écrire ce JS générique
 * @param string $valeur
 * @param string $guillemet
 * @param string $negation
 * @param array $saisie
**/
function saisies_afficher_si_js_fichiers($parse, $saisies_form) {
	if (!$parse['total']) {
		return '';
	}
	$champ = $parse['champ'];
	$saisie = $saisies_form[$champ];
	$negation = $parse['negation'];
	$operateur = $parse['operateur'];
	$valeur = $parse['valeur'];

	$nb_fichiers = isset($saisie['options']['nb_fichiers']) ? $saisie['options']['nb_fichiers'] : 0;

	if (!$nb_fichiers) {
		$nb_fichiers = 1;
	} else {
		$nb_fichiers = intval($nb_fichiers);
	}

	$total = "$(form).find(\"[name^='cvtupload_fichiers_precedents[$champ]']\").length";
	$i = 0;
	while ($i < $nb_fichiers) {
		$total .= " + $(form).find(\"[name^='{$champ}[$i]']\")[0].files.length";
		$i++;
	}
	$total = "($total)";
	$result = "$negation $total $operateur $valeur";
	return $result;
}
